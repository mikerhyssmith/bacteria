var Game = require("../src/game");


describe("calculateNextMove", () => {

    it("should set a live cells state to be dead if it has < 2 live neighbours", () => {
        // given
        const cell = { x: 0, y: 0, state: true }
        const surroundingCells = [{ x: 0, y: 1, state: false }, { x: 1, y: 0, state: false }, { x: 1, y: 1, state: false }]

        // when
        const result = Game.calculateNextMove(cell, surroundingCells);

        // then
        expect(result).toEqual({ x: 0, y: 0, state: false });
    });

    it("should maintain a live cells state as live if it has 2/3 live neighbours", () => {
        // given
        const cell = { x: 0, y: 0, state: true }
        const surroundingCells = [{ x: 0, y: 1, state: true }, { x: 1, y: 0, state: true }, { x: 1, y: 1, state: false }]

        // when
        const result = Game.calculateNextMove(cell, surroundingCells);

        // then
        expect(result).toEqual({ x: 0, y: 0, state: true });
    });

    it("should set a live cells state dead if it has >3 live neighbours", () => {
        // given
        const cell = { x: 0, y: 0, state: true }
        const surroundingCells = [{ x: 0, y: 1, state: true }, { x: 1, y: 0, state: true }, { x: 1, y: 1, state: true }, { x: 1, y: 2, state: true }]

        // when
        const result = Game.calculateNextMove(cell, surroundingCells);

        // then
        expect(result).toEqual({ x: 0, y: 0, state: false });
    });

    it("should set a dead cells state alive if it has 3 live neighbours", () => {
        // given
        const cell = { x: 0, y: 0, state: false }
        const surroundingCells = [{ x: 0, y: 1, state: true }, { x: 1, y: 0, state: true }, { x: 1, y: 1, state: true }, { x: 1, y: 2, state: false }]

        // when
        const result = Game.calculateNextMove(cell, surroundingCells);

        // then
        expect(result).toEqual({ x: 0, y: 0, state: true });
    });
});


describe("play", () => {

    it("should create grid and calculate next position of all cells given valid inputs", () => {
        // when 
        const output = Game.play([{ x: 1, y: 2 }, { x: 2, y: 2 }, { x: 3, y: 2 }]);

        // then
        expect(output).toEqual([{ x: 2, y: 3, state: true },
                                { x: 2, y: 2, state: true },
                                { x: 2, y: 1, state: true },
                                { x: 1, y: 3, state: false },
                                { x: 1, y: 1, state: false },
                                { x: 0, y: 3, state: false },
                                { x: 0, y: 2, state: false },
                                { x: 0, y: 1, state: false },
                                { x: 3, y: 3, state: false },
                                { x: 3, y: 2, state: false },
                                { x: 3, y: 1, state: false },
                                { x: 1, y: 2, state: false },
                                { x: 4, y: 3, state: false },
                                { x: 4, y: 2, state: false },
                                { x: 4, y: 1, state: false }]);
    });
});