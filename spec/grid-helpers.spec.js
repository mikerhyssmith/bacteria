var GridHelpers = require("../src/grid-helpers");

describe("grid helpers", () => {

    describe("get surrounding coordinates", () => {

        it("should return all surrounding coordinates", () => {
            // given

            // when
            const coordinates = GridHelpers.getSurroundingCoordinates({x: 1, y: 1});

            // then 
            expect(coordinates).not.toContain({x:1, y:1});
            expect(coordinates).toContain({x:0, y:0});
            expect(coordinates).toContain({x:0, y:1});
            expect(coordinates).toContain({x:0, y:2});
            expect(coordinates).toContain({x:1, y:0});
            expect(coordinates).toContain({x:1, y:2});
            expect(coordinates).toContain({x:2, y:0});
            expect(coordinates).toContain({x:2, y:1});
            expect(coordinates).toContain({x:2, y:2});
            
        });

    });

    describe("get coordinates with potential change", () => {
        it("should return unique coordinates given a list of input coordinates", () => {
            // given
            const inputCoordinates = [ {x: 1, y: 2}, {x: 2, y: 2} ];

            // when
            const output = GridHelpers.getCoordinatesWithPotentialChange(inputCoordinates);

            // then
            expect(output.length).toEqual(12);
            expect(output).toContain({x:0, y:2});
            expect(output).toContain({x:0, y:1});
            expect(output).toContain({x:0, y:3});
            expect(output).toContain({x:1, y:1});
            expect(output).toContain({x:2, y:1});
            expect(output).toContain({x:1, y:2});
            expect(output).toContain({x:1, y:3});
            expect(output).toContain({x:2, y:2});
            expect(output).toContain({x:2, y:3});
            expect(output).toContain({x:3, y:1});
            expect(output).toContain({x:3, y:2});
            expect(output).toContain({x:3, y:3});
        });
    });
});