var _ = require("lodash/core");

var GridHelpers = require("./grid-helpers");

exports.play = (inputCoordinates) => {
    const outputCoordinates = [];

    const coordinatesWithPotentialChange = GridHelpers.getCoordinatesWithPotentialChange(inputCoordinates);

    coordinatesWithPotentialChange.forEach(coordinate => {
        const surroundingCells = GridHelpers.getSurroundingCoordinates(coordinate)
                                            .map(coordinate => addCurrentStateToCoordinate(coordinate, inputCoordinates));
        const coordinateWithState = addCurrentStateToCoordinate(coordinate, inputCoordinates);

        outputCoordinates.push(this.calculateNextMove(coordinateWithState, surroundingCells));
    });

    return outputCoordinates;

}

exports.calculateNextMove = (coordinate, surroundingCoordinates) => {
    const surroundingLiveCells = surroundingCoordinates.reduce((sum, value) => {
        return value.state ? sum + 1 : sum; 
    }, 0)

    const newState = coordinate.state ? (surroundingLiveCells === 2 || surroundingLiveCells == 3) : surroundingLiveCells == 3;

    coordinate.state = newState;
    return coordinate;
}


function addCurrentStateToCoordinate(coordinate, inputCoordinates) {
    coordinate.state = _.find(inputCoordinates, coordinate) != null;
    return coordinate;
}