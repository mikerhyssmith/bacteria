var _ = require('lodash/core');


exports.getSurroundingCoordinates = (coordinate) => {
    let surroundingCoords = [];

    for (i = -1; i <= 1; i++) {
        for (j = -1; j <= 1; j++) {
            const surroundingCoordinate = { x: coordinate.x - i, y: coordinate.y - j };
            if (!_.isEqual(surroundingCoordinate, coordinate) && coordinateIsValid(surroundingCoordinate)) {
                surroundingCoords.push(surroundingCoordinate);
            }
        }
    }

    return surroundingCoords;
}

function coordinateIsValid(coordinate) {
    return coordinate.x >= 0 && coordinate.y >= 0 ;
}


exports.getCoordinatesWithPotentialChange = (inputCoordinates) => {
    const coordinatesWithPossibleChange = inputCoordinates.map(coordinate => this.getSurroundingCoordinates(coordinate));
    const flattenedCoordinatesWithPossibleChange =  _.flatten(coordinatesWithPossibleChange).concat(inputCoordinates);
    const uniqueCoordinatesWithPossibleChange = removeDuplicates(flattenedCoordinatesWithPossibleChange);

    return uniqueCoordinatesWithPossibleChange;
}

function removeDuplicates(inputArray) {
    const deDupedArray = [];
    inputArray.forEach(inputItem => {
        if(!_.find(deDupedArray, (element) =>  _.isEqual(element, inputItem) )) {
            deDupedArray.push(inputItem);
        };
    });
    return deDupedArray;
}