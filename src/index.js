var Game = require("./game");
var GridHelpers = require("./grid-helpers");

function start(input) {
    const output = Game.play(input);
    printOutputData(output);
}

function printOutputData(output) {
    output.filter(coordinate => coordinate.state === true)
        .forEach(coordinate => {
            console.log(coordinate.x + "," + coordinate.y);
        });
    console.log("end");
}


process.stdin.resume();
process.stdin.setEncoding('utf8');
var input = [];

process.stdin.on('data', function (text) {
    if (text.startsWith('end')) {
        start(input);
    } else if (text.match(/\d+,\d+/) != null) {
        const stringCoordinates = text.split(",");
        input.push({ x: Number(stringCoordinates[0]), y: Number(stringCoordinates[1]) });
    }
});

